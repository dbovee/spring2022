<?php
    $SEC_PER_MIN = 60;
    $SEC_PER_HOUR = 60 * $SEC_PER_MIN;
    $SEC_PER_DAY = 24 * $SEC_PER_HOUR;
    $SEC_PER_YEAR = 365 * $SEC_PER_DAY;

    $NOW = time();
    $NEXT_DECADE = mktime(0,0,0,1,1,2030);

    $seconds = $NEXT_DECADE - $NOW;

    //--how many years
    $years = floor($seconds / $SEC_PER_YEAR);
    //--remove years in seconds from total seconds
    $seconds = $seconds - ($SEC_PER_YEAR*years);

    //--how many days?
    $days = floor($seconds/$SEC_PER_DAY);
    //--remove days in seconds from total seconds
    $seconds = $seconds - ($SEC_PER_DAY*$days);

    //--how many hours?
    $hours = floor($seconds/$SEC_PER_HOUR);
    //--remove hours in seconds from total seconds
    $seconds = $seconds - ($SEC_PER_HOUR*$hours);

    //--how many minutes?
    $minutes = floor($seconds/$SEC_PER_MIN);
    //--remove minutes in seconds from total seconds
    $seconds = $seconds - ($SEC_PER_MIN*$days);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Andrew's Timer</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />
</head>
<body>
<header>
    <?php include('../Template/header.php'); ?>
</header>
<nav>
    <?php include('../Template/nav.php'); ?>
</nav>
<main>
    <h1>Countdown Timer</h1>
    <p><?= date("Y-m-d H:i:s") ?></p>
    <p>NOW: <?= $NOW ?></p>
    <p>Next Decade: <?= $NEXT_DECADE ?></p>
    <p>Years: <?= $years ?> | Days: <?= $days ?> | Hours: <?= $hours ?> | Minutes: <?= $minutes ?> | Seconds: <?= $seconds ?> </p>
</main>
<footer>
    <?php include('../Template/footer.php'); ?>
</footer>
</body>
</html>