<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Andrew's Homepage</title>
        <link rel="stylesheet" type="text/css" href="css/base.css" />
    </head>
    <body>
        <header>
            <?php include('Template/header.php'); ?>
        </header>
        <nav>
            <?php include('Template/nav.php'); ?>
        </nav>
        <main>
            <img src="img/monke.jpg" alt="Picture"/>
            <p>Vivamus mattis vel arcu ut maximus. Aliquam ac placerat nibh. Quisque vitae eros eget purus cursus rutrum sit amet ut orci. Ut tellus dui, iaculis nec nulla vel, lacinia porttitor diam. Nullam at velit non velit eleifend dictum. Nulla facilisi. In cursus felis tincidunt mattis pretium. Aliquam nulla erat, eleifend a sodales id, aliquet sed ex. Donec efficitur eros et justo porttitor, a tincidunt dolor tempus. Vivamus id commodo diam. Proin nec neque rutrum diam laoreet cursus dapibus eget tellus. Aliquam semper tortor massa, ut ornare velit convallis vitae. Quisque consequat elit mollis fermentum malesuada. Vestibulum arcu odio, tempor nec molestie et, lacinia in urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </main>
        <footer>
            <?php include('Template/footer.php'); ?>
        </footer>
    </body>
</html>